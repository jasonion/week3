<?php
    include('php/functions.php');
    
    //Variables from a form
    $name = $_GET['name'];
    $link = $_GET['link'];
    
    function replaceLink($name, $link) {
        
        $answer == "";
        
        if (isset($name) && isset($link)) {
            $answer = "<a class=\"links\" href=\"".$link."\" target=\"_blank\">".$name."</a>";
        }
        
        if (($name == "") && ($link == "")) {
            $answer = "<p class=\"links\" >Please fill in the boxes below</p>";
        }
      
        return $answer;
    }
?>
<!doctype html>
<html>
    <head>
        <title><?php echo titleSite(); ?></title>
        <link rel="stylesheet" type="text/css" href="css/main.css" >
    </head>
    <body>
        <header class="left">
            <h1><?php echo titleSite(); ?></h1>
            <h2><?php echo subTitleSite(); ?></h2>
        </header>
        <header class="right">
            <?php echo socialMediaIcons(); ?>
        </header>
        <nav>
            <?php echo showNavBar($navArray);  ?>
        </nav>
        <section class="bodyText">
            
            <?php echo replaceLink($name, $link);  ?>
            
            <br><br>
            
            <form action="indexPHP.php" method="GET">
                <fieldset>
                    <legend>Change this link</legend>
                    <input type="text" name="name" value="" placeholder="Type in the name of the link">
                    <input type="url" name="link" value="" placeholder="Type in the URL">
                    <input type="submit" value="Change Link">
                </fieldset>
            </form>
            
        </section>
    </body>
</html>
