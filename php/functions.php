<?php 
    //Variables to be accessed in other functions
    $navArray = ["Home", "Google", "Boppoly", "Microsoft"]; //change css
   
    //Some content for the page
    function titleSite() {
        return "Welcome to my website";
    }
    
    function subTitleSite() {
        return "Just some PHP stuff";
    }
    
    //Navbar here, saves me repeating myself
    //Note that once you set the number of items in the array - that number needs to be updated in the css
    function showNavBar($links) {
        
        $nav = "<ul>";
        
        foreach($links as $x) {
            $nav .= "<li>$x</li>";
        }
        $nav .= "</ul>";
    
        return $nav; 
    }
    
    function socialMediaIcons() {
        
        $socialmedia = ["facebook", "twitter", "linkedin", "pinterest"];
        $sociallinks = ["https://facebook.com", "https://twitter.com", "https://linkedin.com", "https://pinterest.com"];
        $socialcolors = ["3B5998", "55acee", "0077b5", "bd081c"];
        
        $allimages = "";
        
        for ($i = 0; $i < count($socialmedia); $i++) 
        {
            $allimages .= "<img class=\"contact-icons ".$socialmedia[$i]." \" src=\"images/".$socialmedia[$i].".png\" alt=\"".$socialmedia[$i]."\">";
        }
        
        return $allimages;
        
    }
    
    function replaceWord($word) {
                
        $newWord = str_replace("still write text here", "change things here", $word);
        
        return $newWord;
    }
    
 
?>